import firebase from "firebase/app"; // for config
import "firebase/auth"; // for firebase login
import "firebase/firestore"; // for firebase storage

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDnKB_zWgIX_su3MEcOY3QN6TIodOGOQe0",
  authDomain: "login-b891f.firebaseapp.com",
  projectId: "login-b891f",
  storageBucket: "login-b891f.appspot.com",
  messagingSenderId: "598614404780",
  appId: "1:598614404780:web:9185cfdd2ba9e9c484b909",
  measurementId: "G-PHH7BQJNT3",
};

firebase.initializeApp(firebaseConfig); // init the app hear from the fire base

export const auth = firebase.auth(); // create auth instance and export

export const firestore = firebase.firestore(); // create firestore DB instance and export

const googleLoginProvider = new firebase.auth.GoogleAuthProvider(); // provides a google auth

export const signInWithGoogle = () => auth.signInWithPopup(googleLoginProvider); // set as popup google login

export const generateUserDocument = async (user) => {
  if (!user) return;
  const userRef = firestore.doc(`users/${user.uid}`);
  const snapshot = await userRef.get();
  if (!snapshot.exists) {
    const { email } = user;
    try {
      await userRef.set({
        email,
      });
    } catch (error) {
      console.error("Error creating user document", error);
    }
  }
  return getUserDocument(user.uid);
};

const getUserDocument = async (uid) => {
  if (!uid) return null;
  try {
    const userDocument = await firestore.doc(`users/${uid}`).get();
    return {
      uid,
      ...userDocument.data(),
    };
  } catch (error) {
    console.error("Error fetching user", error);
  }
};
