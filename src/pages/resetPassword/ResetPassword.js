import React, { useState } from "react";
import "./ResetPassword.css";
import { Link } from "react-router-dom";
import { auth } from "../../firebase.js";
const ResetPassword = () => {
  const [email, setEmail] = useState("");
  const [emailHasBeenSent, setEmailHasBeenSent] = useState(false);
  const [error, setError] = useState(null);

  const onChangeHandler = (event) => {
    const { name, value } = event.currentTarget;
    if (name === "reset") {
      setEmail(value);
    }
  };

  const sendResetEmail = (event) => {
    event.preventDefault();
    auth
      .sendPasswordResetEmail(email)
      .then(() => {
        setEmailHasBeenSent(true);
        setTimeout(() => {
          setEmailHasBeenSent(false);
        }, 3000);
      })
      .catch(() => {
        setError("Error resetting password");
      });
  };

  return (
    <div className="reset__root">
      <div className="bg__grid">
        {error !== null && <div>{error} </div>}
        <div className="center__grid">
          <form className="center__grid" onSubmit={sendResetEmail}>
            <label htmlFor="reset" className="reset__label">
              Enter Mail Id:
            </label>
            <input
              type="email"
              name="reset"
              id="reset"
              className="reset"
              onChange={onChangeHandler}
            />
            <input type="submit" value="Send mail" className="sent__email" />
          </form>
          <div className="links">
            <div className="signup">
              <Link to="/">Signup</Link>
            </div>

            <div className="signin">
              <Link to="/signin">SignIn</Link>
            </div>
          </div>
          {emailHasBeenSent !== false && (
            <div className="status">Mail sent please check the mail</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default ResetPassword;
