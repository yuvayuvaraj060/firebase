import React, { useState } from "react";
import "./Auth.css";
import { useHistory, Link, useLocation } from "react-router-dom";
import {
  signInWithGoogle,
  generateUserDocument,
  auth,
} from "../../firebase.js";

const Auth = () => {
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [error, setError] = useState(null);

  const history = useHistory();
  const { pathname } = useLocation();

  const onChangeHandler = (e) => {
    const { name, value } = e.currentTarget;

    if (name === "name") setEmail(value);
    if (name === "password") setPass(value);
  };

  const createUserWithEmailAndPasswordHandler = async (
    event,
    email,
    password
  ) => {
    event.preventDefault();

    try {
      const { user } = await auth
        .createUserWithEmailAndPassword(email, password)
        .then((result) => {
          console.log("🚀 ~ file: auth.js ~ line 36 ~ .then ~ result", result);
          history.push("/user");
          return result;
        })
        .catch((err) => {
          console.log("🚀 ~ file: auth.js ~ line 38 ~ .then ~ err", err);
          setError(err.message);
          return err;
        });
      generateUserDocument(user);
    } catch (error) {
      console.log(error);
    }

    setEmail("");
    setPass("");
  };

  const signInWithEmailAndPasswordHandler = (event, email, password) => {
    event.preventDefault();
    auth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        history.push("/user");
      })
      .catch((error) => {
        setError("Error signing in with password and email!");
        console.error("Error signing in with password and email", error);
      });
  };

  return (
    <div className="auth__root">
      <div className="auth__bg">
        {error !== null && <div className="error">{error}error</div>}
        <form
          className="auth__form"
          onSubmit={
            pathname === "/signin"
              ? (e) => signInWithEmailAndPasswordHandler(e, email, pass)
              : (e) => createUserWithEmailAndPasswordHandler(e, email, pass)
          }
        >
          <div className="email__filed">
            <label htmlFor="name" className="email__label">
              Email:
            </label>
            <input
              type="email"
              name="name"
              id="name"
              className="name"
              onChange={onChangeHandler}
            />
          </div>

          <div className="password__filed">
            <label htmlFor="password" className="password__label">
              Password:
            </label>
            <input
              type="password"
              name="password"
              id="password"
              className="password"
              onChange={onChangeHandler}
            />
          </div>

          <div className="submit-btn">
            {pathname === "/signin" ? (
              <input type="submit" value="Sign In" className="submit-btn" />
            ) : (
              <input type="submit" value="Sign UP" className="submit-btn" />
            )}
          </div>

          <div className="social__login">
            <button
              className="google__login"
              onClick={(e) => {
                e.preventDefault();
                signInWithGoogle()
                  .then((result) => {
                    history.push("/user");
                  })
                  .catch((err) => {
                    setError(err);
                  });
              }}
            >
              Google
            </button>
            <button className="facebook__login">FaceBook</button>
          </div>

          <div className="create_new__user">
            {pathname === "/signin" ? (
              <>
                <div style={{ display: "inline-block" }}>
                  Don't have an account?
                </div>
                <Link to="/">SignUp</Link>
              </>
            ) : (
              <>
                <div style={{ display: "inline-block" }}>You Have account?</div>
                <Link to="/signin">SignIn</Link>
              </>
            )}
          </div>

          <div className="forget__pass">
            <Link to="reset">Forget Password</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Auth;
