import React, { useContext } from "react";
import { userContext } from "../../providers/UserProviders.js";

const DisplayUser = () => {
  const user = useContext(userContext);
  return <div className="">{JSON.stringify(user)}</div>;
};

export default DisplayUser;
