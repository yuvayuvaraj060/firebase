import { Switch, BrowserRouter, Route } from "react-router-dom";
import Reset from "./pages/resetPassword/ResetPassword.js";
import Auth from "./pages/auth/Auth.js";
import DisplayUser from "./pages/displayUser/DisplayUser.js";
import UserProvider from "./providers/UserProviders.js";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          {/* application */}
          <Route
            path="/user"
            render={() => (
              <UserProvider>
                <DisplayUser />
              </UserProvider>
            )}
          />

          {/* Password Reset */}
          <Route path="/reset" render={() => <Reset />} />
          {/* Profile Page */}

          {/* SignIn */}
          <Route exact path="/signin" render={() => <Auth />} />

          {/* SignUp */}
          <Route exact path="/" render={() => <Auth />} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
