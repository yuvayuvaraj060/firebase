import React, { createContext, useEffect, useState } from "react";
import { auth, generateUserDocument } from "../firebase";

export const userContext = createContext({ user: null });

const UserProviders = (props) => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    auth.onAuthStateChanged(async (userAuth) => {
      const user = await generateUserDocument(userAuth);
      setUser(user);
    });
  }, []);

  console.log("props.children", props.children);

  return (
    <userContext.Provider value={user}>{props.children}</userContext.Provider>
  );
};

export default UserProviders;
